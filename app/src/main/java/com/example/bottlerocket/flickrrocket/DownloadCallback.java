package com.example.bottlerocket.flickrrocket;


import android.graphics.Bitmap;
import android.net.NetworkInfo;

public interface DownloadCallback {
    void updateFromDownload(Exception e);

    void updateImageFromDownload(Bitmap image);

    NetworkInfo getActiveNetworkInfo();

    void finishDownloading();

    void finishImageDownloading();
}
