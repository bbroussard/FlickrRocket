package com.example.bottlerocket.flickrrocket;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFragment extends Fragment {
    public static final String TAG = "DownloadFragment";
    private static final String URL_KEY = "url";

    private DownloadCallback mCallback;
    private DownloadSearchResultsTask mDownloadSearchResultsTask;
    private DownloadImageTask mDownloadImageTask;
    private String mUrlString = null;
    private static String[] mImageUrlString;
    private int mImageUrlCount = 0;
    private static Bitmap bitmapImage;

    // Empty constructor necessary for DownloadFragment.getInstance
    public DownloadFragment() {
    }

    public static DownloadFragment getInstance(FragmentManager fragmentManager, String url) {
        DownloadFragment downloadFragment = (DownloadFragment) fragmentManager.findFragmentByTag(DownloadFragment.TAG);
        if (downloadFragment == null) {
            downloadFragment = new DownloadFragment();
            Bundle args = new Bundle();
            args.putString(URL_KEY, url);
            downloadFragment.setArguments(args);
            fragmentManager.beginTransaction().add(downloadFragment, TAG).commit();
        }

        return downloadFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUrlString = getArguments().getString(URL_KEY);
        }
        // Ensures this fragment is retained across user configuration changes in host Activity
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (DownloadCallback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelDownload();
    }

    /**
     * Initiates asynchronous download of the API call's JSON data.
     * @param url The API call's URL as passed from MainActivity after a search.
     */
    public void startDownload(String url) {
        mUrlString = url;
        mImageUrlCount = 0;
        cancelDownload();
        mDownloadSearchResultsTask = new DownloadSearchResultsTask(mCallback);
        mDownloadSearchResultsTask.execute(mUrlString);
    }

    /**
     * Initiates asynchronous download of images individually by sequentially passing
     * an image URL stored in the mImageUrlString array, cycling back to the beginning
     * when reaching the end of the array.
     */
    public void startImageDownload() {
        cancelImageDownload();
        mDownloadImageTask = new DownloadImageTask(mCallback);
        mDownloadImageTask.execute(mImageUrlString[mImageUrlCount]);
        if (mImageUrlCount == mImageUrlString.length - 1) {
            mImageUrlCount = 0;
        } else {
            mImageUrlCount++;
        }
    }

    public void cancelDownload() {
        if (mDownloadSearchResultsTask != null) {
            mDownloadSearchResultsTask.cancel(true);
        }
    }

    public void cancelImageDownload() {
        if (mDownloadImageTask != null) {
            mDownloadImageTask.cancel(true);
        }
    }

    /**
     * #3 & #6: Downloads search results as JSON data, and later decodes downloaded bitmaps.
     * @param url: passed from MainActivity to download JSON data.
     *           Afterward, passed from mImageUrlString array to download individual bitmaps.
     * @throws IOException: in the event the server deviates from the expected response.
     * @throws JSONException: in the event a JSONObject or JSONArray is not found.
     */
    private static void downloadUrl(URL url) throws IOException, JSONException {
        InputStream stream = null;
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        String result;
        try {
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(5000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            stream = connection.getInputStream();
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }
            if (stream != null) {
                result = readStream(stream, 50000);
                // Parse the JSON data and store each image URL in a String array;
                // decode .jpg bitmap otherwise.
                if (result.startsWith("{\"photos\":")) {
                    mImageUrlString = parseJsonData(result);
                } else {
                    bitmapImage = BitmapFactory.decodeStream((InputStream) url.getContent());
                }
            }
        } finally {
            if (stream != null) {
                stream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Converts the contents of the API call to a String.
     */
    public static String readStream(InputStream stream, int maxReadSize) throws IOException {
        Reader reader = new InputStreamReader(stream, "UTF-8");
        char[] rawBuffer = new char[maxReadSize];
        int readSize;
        StringBuilder buffer = new StringBuilder();
        while (((readSize = reader.read(rawBuffer)) != -1) && maxReadSize > 0) {
            if (readSize > maxReadSize) {
                readSize = maxReadSize;
            }
            buffer.append(rawBuffer, 0, readSize);
            maxReadSize -= readSize;
        }
        return buffer.toString();
    }

    /**
     * #4 and #5: Converts returned JSON data to individual JSONObjects, from which relevant
     * property data are obtained and used to build the individual image URLs for further downloading.
     */
    private static String[] parseJsonData(String data) throws JSONException {
        data = data.substring(data.indexOf("["));
        JSONArray photosArray = new JSONArray(data);
        String[] imageUrl = new String[photosArray.length()];
        for (int i = 0; i < photosArray.length(); i++) {
            JSONObject imageMetadata = photosArray.getJSONObject(i);
            int farm      = imageMetadata.getInt("farm");
            int server    = imageMetadata.getInt("server");
            long photoId  = imageMetadata.getLong("id");
            String secret = imageMetadata.getString("secret");
            imageUrl[i]   = "http://farm" + farm + ".static.flickr.com/" + server + "/" +
                    photoId + "_" + secret + "_h.jpg";
        }
        return imageUrl;
    }

    private static class DownloadSearchResultsTask extends AsyncTask<String, Void, DownloadSearchResultsTask.Result> {
        private DownloadCallback mCallback;

        DownloadSearchResultsTask(DownloadCallback callback) {
            setCallback(callback);
        }

        void setCallback(DownloadCallback callback) {
            mCallback = callback;
        }

        static class Result {
            static Exception mException;

            Result(Exception exception) {
                mException = exception;
            }
        }

        @Override
        protected void onPreExecute() {
            if (mCallback != null) {
                NetworkInfo networkInfo = mCallback.getActiveNetworkInfo();
                if (networkInfo == null || !networkInfo.isConnected() ||
                        (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                                networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                    cancel(true);
                }
            }
        }

        protected DownloadSearchResultsTask.Result doInBackground(String... urls) {
            Result result = null;
            if (!isCancelled() && urls != null && urls.length > 0) {
                String urlString = urls[0];
                try {
                    URL url = new URL(urlString);
                    downloadUrl(url);
                    if (mImageUrlString.length == 0) {
                        throw new IOException("No photos match.");
                    }
                } catch (Exception e) {
                    result = new Result(e);
                }
            }
            return result;
        }

        protected void onPostExecute(Result result) {
            if (result == null && mCallback != null) {
                mCallback.updateFromDownload(null);
                mCallback.finishDownloading();
            } else if (result != null) {
                mCallback.updateFromDownload(Result.mException);
                mCallback.finishDownloading();
            }
        }
    }

    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private DownloadCallback mCallback;

        DownloadImageTask(DownloadCallback callback) {
            setCallback(callback);
        }

        void setCallback(DownloadCallback callback) {
            mCallback = callback;
        }

        @Override
        protected void onPreExecute() {
            if (mCallback != null) {
                NetworkInfo networkInfo = mCallback.getActiveNetworkInfo();
                if (networkInfo == null || !networkInfo.isConnected() ||
                        (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                         networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                    mCallback.updateImageFromDownload(null);
                    cancel(true);
                }
            }
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            if (!isCancelled() && urls != null && urls.length > 0) {
                String urlString = urls[0];
                try {
                    URL url = new URL(urlString);
                    downloadUrl(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return bitmapImage;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null && mCallback != null) {
                mCallback.updateImageFromDownload(bitmap);
                mCallback.finishImageDownloading();
            }
        }
    }
}