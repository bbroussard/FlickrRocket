package com.example.bottlerocket.flickrrocket;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements DownloadCallback {
    private DownloadFragment mDownloadFragment;

    /* UI elements */
    private boolean mDownloading = false;
    private boolean mSearchResultsDownloaded = false;
    private ImageView mBitmapView;
    android.support.v7.widget.SearchView searchView;
    ProgressBar progressBar;
    Animation fadeOutAnimation;
    Animation fadeInAnimation;

    String mSearchTag = "";

    // The API call's URL has been corrected because the "http://" variant
    // threw a FileNotFoundException.
    String flickrUrl = "https://api.flickr.com/services/rest/?format=json" +
            "&sort=random&method=flickr.photos.search&tags=rocket&tag_mode=all" +
            "&api_key=0e2b6aaf8a6901c264acb91f151a3350&nojsoncallback=1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mBitmapView = findViewById(R.id.flickrView);
        progressBar = findViewById(R.id.progressIndicator);
        mDownloadFragment = DownloadFragment.getInstance(getSupportFragmentManager(), flickrUrl);
        fadeOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        fadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
    }

    /**
     * EC #3: Sets up the searchView widget programmatically, reads input query string and assigns
     * it to a variable used to build the resulting API URL. The URL is then passed to the
     * DownloadFragment via startDownload.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (android.support.v7.widget.SearchView) searchItem.getActionView();
        searchView.setQueryHint(getString(R.string.search_tag));
        searchView.setImeOptions(3);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSearchResultsDownloaded = false;
                mSearchTag = query.trim();
                flickrUrl = "https://api.flickr.com/services/rest/?format=json" +
                        "&sort=random&method=flickr.photos.search&tags=" + mSearchTag +
                        "&tag_mode=all&api_key=0e2b6aaf8a6901c264acb91f151a3350&nojsoncallback=1";
                searchView.clearFocus();
                startDownload(flickrUrl);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });

        super.onOptionsMenuClosed(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mSearchResultsDownloaded && !mDownloading && mDownloadFragment != null) {
            startDownload(flickrUrl);
        }
    }

    /**
     * EC #2: Initiates download of JSON data and calls the progress indicator to display itself
     * while background operations are occurring.
     */
    private void startDownload(String url) {
        mDownloading = true;
        mDownloadFragment.startDownload(url);
        progressBar.startAnimation(fadeInAnimation);
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * #7 and EC #1: Initiates download of next image in the set via DownloadFragment's
     * DownloadImageTask, calls the display of the progress indicator, and runs the first half
     * of the image crossfade animation sequence.
     */
    public void onClick(View view) {
        if (mSearchResultsDownloaded) {
            progressBar.startAnimation(fadeInAnimation);
            progressBar.setVisibility(View.VISIBLE);
            mDownloadFragment.startImageDownload();
            mBitmapView.startAnimation(fadeOutAnimation);
            mBitmapView.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateFromDownload(Exception e) {
        if (e == null) {
            mSearchResultsDownloaded = true;
            mDownloadFragment.startImageDownload();
        } else if (e.toString().equals("java.io.IOException: No photos match.")) {
            Toast.makeText(this, "No matching results. " +
                    "Try searching with another tag!", Toast.LENGTH_SHORT).show();
            finishDownloading();
            progressBar.startAnimation(fadeOutAnimation);
            progressBar.setVisibility(View.GONE);
        } else {
            Toast.makeText(this, "Image could not download. " +
                    "Please try again later.", Toast.LENGTH_SHORT).show();
            finishDownloading();
            progressBar.startAnimation(fadeOutAnimation);
            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void updateImageFromDownload(Bitmap image) {
        if (image == null) {
            Toast.makeText(this, "Image not found. Please try again later.", Toast.LENGTH_SHORT).show();
        }
        mBitmapView.setVisibility(View.VISIBLE);
        mBitmapView.startAnimation(fadeInAnimation);
        mBitmapView.setImageBitmap(image);
    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo();
    }


    @Override
    public void finishDownloading() {
        mDownloading = false;
        if (mDownloadFragment != null) {
            mDownloadFragment.cancelDownload();
        }
    }

    public void finishImageDownloading() {
        progressBar.startAnimation(fadeOutAnimation);
        progressBar.setVisibility(View.GONE);
        if (mDownloadFragment != null) {
            mDownloadFragment.cancelImageDownload();
        }
    }
}

